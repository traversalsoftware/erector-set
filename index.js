var argv = require('minimist')(process.argv.slice(2));

const metalsmith = require('metalsmith')
const imagemin = require('metalsmith-imagemin');
const markdown = require('metalsmith-markdown')
const layouts = require('metalsmith-layouts')
const sass = require('metalsmith-sass');
const watch = require('metalsmith-watch');
const serve = require('metalsmith-serve');
const Promise = require('bluebird');
var Rsync = require('rsync');
var SSH = require('node-sshclient').SSH;

if (!argv['_'][0] || !argv['site']) {
  console.log('You have to specify and action {new, server, build, deploy} and a site');
  console.log('');
  console.log('Usage:');
  console.log('{new, server, build, deploy} --site="site_name" --from="for a new site the template site to use"');
}

switch (argv['_'][0]) {
  case 'build':
    build(argv['site'])
  break;
  case 'server':
    server(argv['site'])
  break;
  case 'deploy':
    build(argv['site']).then(function () {
      deploy(argv['site'])
    })
  break;
  case 'new':
    newSite(argv['site'], argv['from'])
  break;
  default:
      console.log('Not a valid action');
}

function build(site) {
  function addName(files, metalsmith, done){
    var metadata = metalsmith.metadata();

    metadata['siteName'] = site;
    done();
  }

  return new Promise(function (resolve, reject) {
    metalsmith(__dirname + '/' + (argv.site || '.site_template'))
      .use(markdown())
      .use(addName)
      .use(layouts('dot'))
      .use(sass({outputStyle: "expanded"}))
      .use(imagemin({
        optimizationLevel: 3,
        svgoPlugins: [{ removeViewBox: false }]
      }))
      .build(function(err) {
        if (err) reject (err);
        resolve()
      });
  })
}

function server(site) {
  function addName(files, metalsmith, done){
    var metadata = metalsmith.metadata();

    metadata['siteName'] = site;
    done();
  }

  metalsmith(__dirname + '/' + (argv.site || '.site_template'))
    .use(markdown())
    .use(addName)
    .use(layouts('dot'))
    .use(sass({outputStyle: "expanded"}))
    .use(imagemin({
      optimizationLevel: 3,
      svgoPlugins: [{ removeViewBox: false }]
    }))
    .use(
      watch({
        paths: {
          "${source}/**/*": true,
          "layouts/**/*": "**/*.md",
        },
        livereload: true,
      })
    )
    .use(serve({}))
    .build(function(err) {
      if (err) throw err;
    });
}

function deploy(site) {
  var ssh = new SSH({
    hostname: '162.243.71.159'
    , user: 'root'
    , port: 22
  });


    // Build the command
  var rsync = new Rsync()
    .shell('ssh')
    .flags('az')
    .source(__dirname + '/' + site + '/build/*')
    .destination('root@162.243.71.159:/var/www/experiments/' + site);

  // Execute the command
  rsync.execute(function(error, code, cmd) {
    if (error) {
      console.error(error)
      return;
    }

    ssh.command('chown', '-R www-data /var/www/experiments/' + site, function(procResult) {
      console.log(procResult.stdout);
      console.log('done')
    });
  });
}

function newSite(site, f) {
  var from = f || '.site_template';
    // Build the command
  var rsync = new Rsync()
    .shell('bash')
    .flags('az')
    .source(__dirname + '/' + from + '/*')
    .destination(__dirname + '/' + site);

  // Execute the command
  rsync.execute(function(error, code, cmd) {
    if (error) {
      console.error(error)
      return;
    }

    console.log('done')
  });
}
