function setupListen() {
  $(':file').on('change', function (evt) {
    var label = $(this).parent();
    label.text("Selected: " + this.files[0].name);
    label.append(this);

  })

  $('input[type=submit]').on('click', function (event) {
    const that = this;

    var $form = $(this).parents('form');

    var validities = $form.find('[required=true]').map(function (carry, val) {
      return ($(this).val() !== '')
    }).toArray();

    valid = validities.reduce(function (carry, val) {
      return carry && val
    }, true);

    if (!valid) {
      alert('Some required fields are missing');
      return;
    }

    event.preventDefault();

    var formData = new FormData($(this).parents('form')[0]);

    $.ajax({
        url: $(this).parents('form').attr('action'),
        type: 'POST',
        data: formData,
        success: function (data) {
          console.log($(that));

        },
        cache: false,
        contentType: false,
        processData: false
    });

    var height = $(this).parents('div').outerHeight();
    var width = $(this).parents('div').outerWidth();

    $(this).parents('div')
      .css('min-height', height + 'px')
      .css('max-width', width + 'px')

    $form
      .fadeOut(400, function () {
        console.log($form);

        $form.replaceWith('<h2 class="confirm">'+that.dataset.confirm+'</h2>')

        $('.confirm')
          .fadeIn()
      })
  })
}

$(function () {
  setupListen()
})
