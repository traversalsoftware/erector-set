---
layout: index.html #Don't change this

uaCode: '' #from google analytics

# Used for social media
title: 'What Up?'
description: 'This is a website share it on social media'

# Actual page text
headline: 'Something is cool'
followOn: 'Buy some shit'
littleText: 'Buy some good shit'
hr: true

overlay: true #show dots over the background image

form:
  - type: "text"
    placeholder: "What is your name?"
    name: "name"
    required: true
  - type: "email"
    placeholder: "Email"
    name: "email"
    required: true
  - type: "file"
    placeholder: "Choose a File"
    types:
      - "pdf"
    required: true
  - type: "submit"
    text: "Let's Go >>"
    confirm: "Awesome, The robot will get to work just as soon as they are ready!"

---
