# Erector Set

## how to get setup from square one

```bash
ssh-keygen -t rsa
curl -L https://raw.githubusercontent.com/beautifulcode/ssh-copy-id-for-OSX/master/install.sh | sh
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.29.0/install.sh | bash
nvm install 4.0 && nvm alias default 4.0.0
git clone https://mwalker1@bitbucket.org/traversalsoftware/erector-set.git
cd erector-set
npm install
ssh-copy-id root@162.243.71.159
```

## Make you first site

```bash
node . new --site="my-first-site"
node . server --site="my-first-site"
```

go to http://localhost:8080/ to see it


## Deploy

```bash
node . deploy --site="my-first-site"
```

go to http://162.243.71.159/my-first-site/ to see it

## To update

```bash
git pull origin master
npm install
```
